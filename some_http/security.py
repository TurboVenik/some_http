import hashlib


def calculate_hash(data: str) -> str:
    data_hash = hashlib.sha256(data.encode()).hexdigest()
    return data_hash
