from sqlalchemy import Column, Integer, String

from some_http.database import Base


# схема базы

class GameModel(Base):  # модели базы данных
    __tablename__ = "game"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)
    price = Column(Integer)


class UserModel(Base):
    __tablename__ = "users" ## userSSSSSS

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True) ## index - B+ дерево, при unique всегда ставить index
    password = Column(String)

