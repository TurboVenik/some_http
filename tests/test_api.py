import json
from typing import Tuple, List, Dict, Any

import pytest
from fastapi.testclient import TestClient

from some_http import models, security
from tests.conftest import db


def create_game(name: str, price: int) -> models.GameModel:
    db_game = models.GameModel(name=name, price=price)
    db.add(db_game)
    db.commit()
    return db_game


def create_user(username: str, password: str) -> models.UserModel:
    hash_pass = security.calculate_hash(password)
    db_user = models.UserModel(username=username, password=hash_pass)
    db.add(db_user)
    db.commit()

    return db_user


def get_one_game(game_id: int) -> models.GameModel:
    result = db.query(models.GameModel).filter(models.GameModel.id == game_id).first()
    return result


def delete_one_game(game_id: int) -> int:
    count = db.query(models.GameModel).filter(models.GameModel.id == game_id).delete(synchronize_session=False)
    db.commit()
    return count


game_id_for_parametrize = [(1), (2), (3), (4)]
user_list = ({"username": "admin", "password": "test"}), ({"username": "test", "password": "admin"}), (
    {"username": "test", "password": "test"})


@pytest.mark.parametrize(
    "games",
    [
        [("igra1", 11), ("igra2", 33)],
        [("igra3", 44), ("igra4", 55), ("igra5", 66)],
        []
    ]
)
def test_get_games_not_empty(client: TestClient, games: List[Tuple[str, int]]):
    # game1 = create_game("game1", 11)
    # game2 = create_game("game2", 22)
    mas = []
    for game in games:
        prep_for_dict = create_game(game[0], game[1])

        responsed_dict = {"id": prep_for_dict.id, "name": prep_for_dict.name, "price": prep_for_dict.price}
        mas.append(responsed_dict)
    print(mas)
    mas = sorted(mas, key=lambda k: k['price'], reverse=True)
    response = client.get("/games")
    assert response.status_code == 200
    assert len(response.json()) == len(mas)
    assert response.json() == mas


def test_post_games_success(client: TestClient):
    create_user("admin", "admin")
    game_data = {"name": "name", "price": 1}
    response = client.post("/games", json=game_data, params={"username": "admin", "password": "admin"})
    games = db.query(models.GameModel).all()
    print(response.status_code, response.json())
    assert response.status_code == 201
    assert len(games) == 1
    assert response.json() == {"id": games[0].id, "name": games[0].name, "price": games[0].price}
    assert response.json()["name"] == game_data["name"]
    assert response.json()["price"] == game_data["price"]


def test_post_games_conflict(client: TestClient):
    game1 = {"name": "name", "price": 50}
    game2 = {"name": "name", "price": 100}
    create_user("admin", "admin")
    response1 = client.post("/games", json=game1, params={"username": "admin", "password": "admin"})
    games = db.query(models.GameModel).all()
    response2 = client.post("/games", json=game2, params={"username": "admin", "password": "admin"})
    if games[0].name == game2["name"] or response1 == response2:
        assert response2.status_code == 409
        assert response2.json() == {"details": f"{games[0].name} is already created"}


# @pytest.mark.parametrize("games", [(["name"], ["name"]),
#                                    (123, 33),
#                                    ({"name"}, "price"),
#                                    ([]),
#                                    ( )
#                                    ])
# def test_post_games_wrong_body(client: TestClient):
#     create_user("admin", "admin")
#     game1 = {"name": 312, "price": "sd"}
#     proper_json = {
#         "name": game1["name"],
#         "price": game1["price"]
#
#     }
#     response = client.post("/games", json=proper_json, params={"username": "admin", "password": "admin"})
#
#     assert response.status_code == 201
#
#     pass


def test_game_id_is_in_db(client: TestClient):
    game1 = create_game("name", 22)
    create_user("admin", "admin")
    preped_for_dict = {"id": game1.id, "name": game1.name, "price": game1.price}
    client.post("/games", json=preped_for_dict, params={"username": "admin", "password": "admin"})
    game_in_db = db.query(models.GameModel).filter(models.GameModel.id == game1.id).first()
    response = client.get("/games/1")

    assert response.status_code == 200
    assert response.json()["id"] == game_in_db.id
    assert game_in_db.id == int(game_in_db.id)
    assert response.json() == preped_for_dict


@pytest.mark.parametrize("game_id", game_id_for_parametrize)
def test_game_id_not_in_db(client: TestClient, game_id: List[Tuple[int]]):
    response = client.get(f"/games/{game_id}")

    assert response.status_code == 404
    assert response.json() == {"details": f"Game with id {game_id} not found"}


def test_delete_game_by_id_success(client: TestClient):
    create_user("admin", "admin")
    game1 = {"name": "name", "price": 20}
    client.post("/games", json=game1, params={"username": "admin", "password": "admin"})
    response = client.delete("/games/1", params={"username": "admin", "password": "admin"})

    assert response.status_code == 200
    assert response.json() == {"details": "Game was deleted successfully"}


@pytest.mark.parametrize("game_id", game_id_for_parametrize)
def test_delete_by_game_id_fail(client: TestClient, game_id: List[Tuple[int]]):
    create_user("admin", "admin")
    response = client.delete(f"/games/{game_id}", params={"username": "admin", "password": "admin"})

    assert response.status_code == 404
    assert response.json() == {"details": f"Game with game id {game_id} was not found"}


@pytest.mark.parametrize("login", user_list)
def test_wrong_user_or_password(client: TestClient, login: List[Tuple[dict]]):
    create_user("admin", "admin")
    game1 = {"name": "name", "price": 20}
    response = client.post("/games", json=game1, params=login)
    assert response.status_code == 401
    assert response.json() == {"details": "Wrong user or password"}


def test_post_games_unprocessable_entity(client: TestClient):
    create_user("admin", "admin")
    response = client.post("/games", json={}, params={"username": "admin", "password": "admin"})
    games = db.query(models.GameModel).all()
    print(response.status_code, response.json())
    assert response.status_code == 422
    assert len(games) == 0

